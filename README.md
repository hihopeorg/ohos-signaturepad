 # signaturepad

 **本项目是基于开源项目signaturepad进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/gcacace/android-signaturepad )追踪到原项目版本**

 #### 项目介绍

 - 项目名称：一个用于绘制平滑签名的库
 - 所属系列：ohos的第三方组件适配移植
 - 功能：绘制平滑签名
 - 项目移植状态：完成
 - 调用差异：无
 - 项目作者和维护人：hihope
 - 联系方式：hihope@hoperun.com
 - 原项目Doc地址：https://github.com/gcacace/android-signaturepad
 - 编程语言：Java
 - 外部库依赖：无
 - 原项目基线版本：v1.3.1 , sha1:729db03fab0da3591f9b0bc7f6c282ac854c2199

 #### 演示效果

 <img src="screenshot/sample.gif"/>
 
 #### 安装教程
 
  1. 编译依赖库har包signaturepad.har。
  2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
  3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。
  ```
  dependencies {
      implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
  	……
  }
  ```
  4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。
 
  方法2.
  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
 ```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
 ```
  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
 ```
 dependencies {
     implementation 'com.github.gcacace.ohos:signaturepad:1.0.0'
 }
 ```

 #### 使用说明

 1. 在xml中引入签名控件

    自定义配置属性。
    penMinWidth -笔划的最小宽度（默认值：3dp）。
    penMaxWidth -笔划的最大宽度（默认值：7dp）。
    penColor -笔触的颜色（默认值：Color.BLACK）。
    velocityFilterWeight -用于根据先前速度修改新速度的权重（默认值：0.9）。
    clearOnDoubleClick -双击清除签名板（默认：false）
 ```xml
     <com.github.gcacace.signaturepad.views.SignaturePad
         ohos:id="$+id:signaturePad"
         ohos:height="0"
         ohos:weight="5"
         app:clearOnDoubleClick="false"
         ohos:background_element="#FFE4DEDE"
         ohos:margin="10fp"
         ohos:width="match_parent"/>
 ```
 2. 配置签名事件侦听器

  ```java
       SignaturePad signaturePad = (SignaturePad) findComponentById(ResourceTable.Id_signaturePad);
            signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                @Override
                public void onStartSigning() {
                    //开始签名
                }

                @Override
                public void onSigned() {
                    //正在签名
                }

                @Override
                public void onClear() {
                    //清除签名板
                }
            });
  ```

 3. 获取签名板内容

   ```java
     PixelMap signatureBitmap = signaturePad.getSignatureBitmap();
   ```

 4. 清除签名板

   ```java
        signaturePad.clear();
   ```

 5. 保存签名到本地

   ```java

   /**
     * 保存
     */
    public boolean save(PixelMap mPixelMap) {
        ImagePacker imagePacker = ImagePacker.create();
        File distDir = getContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        String filePath = distDir.getAbsolutePath() + File.separator + "img.png";
        HiLog.info(label, "path--" + filePath);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            packingOptions.format = "image/jpeg";
            packingOptions.quality = 90;
            imagePacker.initializePacking(bos, packingOptions);
            imagePacker.addImage(mPixelMap);
            imagePacker.finalizePacking();
            byte[] buffer = bos.toByteArray();
            if (buffer != null) {
                File file = new File(filePath);
                if (file.exists()) {
                    file.delete();
                }
                OutputStream outputStream = new FileOutputStream(file);
                outputStream.write(buffer);
                outputStream.close();
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            HiLog.info(label, "IOException");
            return false;
        }
    }
   ```

 #### 版本迭代

 - v1.0.0

  实现功能

   1. 实现平滑签名
   2. 设置画笔颜色
   3. 清除画板
   4. 保存签名至本地



 #### 版权和许可信息
     Copyright 2014-2016 Gianluca Cacace
 
     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at
 
        http://www.apache.org/licenses/LICENSE-2.0
 
     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.



