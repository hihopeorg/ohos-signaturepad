/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.gcacace.signaturepad;

import com.github.gcacace.signaturepad.views.SignaturePad;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.app.Environment;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class SignatureOhosTest {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "SignatureOhosTest");

    @Test
    public void testSaveSignaturePad() {
        Ability ability = EventHelper.startAbility(MainAbility.class);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        EventHelper.inputSwipe(ability, 200, 200, 200, 500, 2000);
        EventHelper.inputSwipe(ability, 200, 500, 800, 500, 2000);
        EventHelper.inputSwipe(ability, 800, 500, 800, 1500, 2000);
        Component save = ability.findComponentById(ResourceTable.Id_bt_save);

        EventHelper.triggerClickEvent(ability, save);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        File distDir = ability.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        String filePath = distDir.getAbsolutePath() + File.separator + "signature.png";
        boolean isExists = new File(filePath).exists();
        Assert.assertEquals(true, isExists);
    }


    @Test
    public void testCleanSignaturePad() {
        Ability ability = EventHelper.startAbility(MainAbility.class);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SignaturePad signaturePad = (SignaturePad) ability.findComponentById(ResourceTable.Id_signaturePad);

        EventHelper.inputSwipe(ability, 200, 200, 200, 500, 2000);

        EventHelper.inputSwipe(ability, 200, 500, 800, 500, 2000);

        EventHelper.inputSwipe(ability, 800, 500, 800, 1500, 2000);


        Component clear = ability.findComponentById(ResourceTable.Id_bt_clear);
        EventHelper.triggerClickEvent(ability, clear);

        Assert.assertEquals(true, signaturePad.getmHasEditState());
    }


}