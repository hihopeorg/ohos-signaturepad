/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.gcacace.signaturepad.sample;

import com.github.gcacace.signaturepad.ResourceTable;
import com.github.gcacace.signaturepad.views.SignaturePad;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class SignaturePadTest {
    Context context;

    @Test
    public void setPenColor() {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
        SignaturePad signaturePad = new SignaturePad(context);
        signaturePad.setPenColor(Color.getIntColor("#000000"));
        int colorValue = signaturePad.getPaint().getColor().getValue();
        Assert.assertEquals(Color.getIntColor("#000000"), colorValue);
    }

    @Test
    public void setPenColorRes() {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
        SignaturePad signaturePad = new SignaturePad(context);
        signaturePad.setPenColorRes(ResourceTable.Color_dk_blue);
        int colorValue = signaturePad.getPaint().getColor().getValue();
        try {
            Assert.assertEquals(context.getResourceManager().getElement(ResourceTable.Color_dk_blue).getColor(), colorValue);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setMinWidth() {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
        SignaturePad signaturePad = new SignaturePad(context);
        signaturePad.setMinWidth(10f);
        Assert.assertEquals(30, signaturePad.getMinWidth());
    }

    @Test
    public void setMaxWidth() {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
        SignaturePad signaturePad = new SignaturePad(context);
        signaturePad.setMaxWidth(10);
        Assert.assertEquals(30, signaturePad.getMaxWidth());
    }

    @Test
    public void clearView() {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
        SignaturePad signaturePad = new SignaturePad(context);
        signaturePad.clearView();
        Assert.assertTrue(signaturePad.isEmpty());
    }


    @Test
    public void isEmpty() {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
        SignaturePad signaturePad = new SignaturePad(context);
        signaturePad.setIsEmpty(true);
        Assert.assertTrue(signaturePad.isEmpty());
    }

    @Test
    public void convertDpToPx() {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
        SignaturePad signaturePad = new SignaturePad(context);
        int px = signaturePad.convertDpToPx(15);
        Assert.assertEquals(45, px);
    }

    @Test
    public void clear() {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
        SignaturePad signaturePad = new SignaturePad(context);
        signaturePad.clear();
        Assert.assertTrue(signaturePad.getmHasEditState());
    }


}