package com.github.gcacace.signaturepad;

import com.github.gcacace.signaturepad.views.SignaturePad;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Environment;
import ohos.bundle.IBundleManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImagePacker;
import ohos.media.image.PixelMap;

import java.io.*;

public class MainAbility extends Ability implements Component.ClickedListener {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "MainAbility");
    /**
     * 签名板
     */
    private SignaturePad signaturePad;

    /**
     * 保存
     */
    private Button save;

    /**
     * 清除
     */
    private Button clear;

    Image image;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        clear = (Button) findComponentById(ResourceTable.Id_bt_clear);
        clear.setClickedListener(this);
        save = (Button) findComponentById(ResourceTable.Id_bt_save);
        save.setClickedListener(this);
        image = (Image) findComponentById(ResourceTable.Id_image);
        signaturePad = (SignaturePad) findComponentById(ResourceTable.Id_signaturePad);
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                new ToastDialog(MainAbility.this).setText("OnStartSigning").show();
            }

            @Override
            public void onSigned() {
                save.setTextColor(new Color(Color.getIntColor("#000000")));
                clear.setTextColor(new Color(Color.getIntColor("#000000")));
            }

            @Override
            public void onClear() {
                save.setTextColor(new Color(Color.getIntColor("#FF5A5656")));
                clear.setTextColor(new Color(Color.getIntColor("#FF5A5656")));
            }
        });
        checkPermission(0);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_bt_clear:
                signaturePad.clear();
                break;
            case ResourceTable.Id_bt_save:
                checkPermission(1);
                break;
            default:
        }
    }

    private void checkPermission(int requestCode) {
        if (verifySelfPermission("ohos.permission.WRITE_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
            // 应用未被授予权限
            if (canRequestPermission("ohos.permission.WRITE_USER_STORAGE")) {
                // 是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
                requestPermissionsFromUser(
                        new String[]{"ohos.permission.WRITE_USER_STORAGE"}, requestCode);
            } else {
                // 显示应用需要权限的理由，提示用户进入设置授权
                HiLog.info(label, "no    canRequestPermission");
            }
        } else {
            if (requestCode == 1) {
                PixelMap signatureBitmap = signaturePad.getSignatureBitmap();
                boolean save = save(signatureBitmap);
                if (save) {
                    new ToastDialog(this).setOffset(0, 150).setText("save is success").show();
                } else {
                    new ToastDialog(this).setOffset(0, 150).setText("save is failed").show();
                }
            }
        }
    }

    /**
     * 保存
     */
    public boolean save(PixelMap mPixelMap) {
        ImagePacker imagePacker = ImagePacker.create();
        File distDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        String name = String.format("Signature_%d.png", System.currentTimeMillis());
        String filePath = distDir.getAbsolutePath() + File.separator + name;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            packingOptions.format = "image/jpeg";
            packingOptions.quality = 90;
            imagePacker.initializePacking(bos, packingOptions);
            imagePacker.addImage(mPixelMap);
            imagePacker.finalizePacking();
            byte[] buffer = bos.toByteArray();
            if (buffer != null) {
                File file = new File(filePath);
                if (file.exists()) {
                    file.delete();
                }
                OutputStream outputStream = new FileOutputStream(file);
                outputStream.write(buffer);
                outputStream.close();
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            HiLog.info(label, "IOException");
            return false;
        }
    }


    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 0:
                // 匹配requestPermissions的requestCode
                if (grantResults.length > 0 && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                    // 权限被授予
                    // 注意：因时间差导致接口权限检查时有无权限，所以对那些因无权限而抛异常的接口进行异常捕获处理
                } else {
                    // 权限被拒绝
                    new ToastDialog(this).setOffset(0, 150).setText("权限被拒绝").show();
                }
                break;
            case 1:
                if (grantResults.length > 0 && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                    PixelMap signatureBitmap = signaturePad.getSignatureBitmap();
                    boolean save = save(signatureBitmap);
                    if (save) {
                        new ToastDialog(this).setOffset(0, 150).setText("save is success").show();
                    } else {
                        new ToastDialog(this).setOffset(0, 150).setText("save is failed").show();
                    }
                } else {
                    // 权限被拒绝
                    new ToastDialog(this).setOffset(0, 150).setText("权限被拒绝").show();
                }
                break;
        }
    }
}
