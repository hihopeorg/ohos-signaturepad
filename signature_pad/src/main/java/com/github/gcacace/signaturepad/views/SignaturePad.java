package com.github.gcacace.signaturepad.views;

import com.github.gcacace.signaturepad.utils.Bezier;
import com.github.gcacace.signaturepad.utils.ControlTimedPoints;
import com.github.gcacace.signaturepad.utils.SvgBuilder;
import com.github.gcacace.signaturepad.utils.TimedPoint;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SignaturePad extends Component implements Component.DrawTask, Component.TouchEventListener, Component.DoubleClickedListener {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "SignaturePad");

    //View state
    private List<TimedPoint> mPoints;
    private boolean mIsEmpty;
    private Boolean mHasEditState;
    private float mLastTouchX;
    private float mLastTouchY;
    private float mLastVelocity;
    private float mLastWidth;
    private RectFloat mDirtyRect;
    private final SvgBuilder mSvgBuilder = new SvgBuilder();

    // Cache
    private List<TimedPoint> mPointsCache = new ArrayList<>();
    private ControlTimedPoints mControlTimedPointsCached = new ControlTimedPoints();
    private Bezier mBezierCached = new Bezier();

    //Configurable parameters
    private int mMinWidth;
    private int mMaxWidth;
    private float mVelocityFilterWeight;
    private OnSignedListener mOnSignedListener;
    private boolean mClearOnDoubleClick;

    //Default attribute values
    private final int DEFAULT_ATTR_PEN_MIN_WIDTH_PX = 3;
    private final int DEFAULT_ATTR_PEN_MAX_WIDTH_PX = 7;
    private final Color DEFAULT_ATTR_PEN_COLOR = Color.BLACK;
    private final float DEFAULT_ATTR_VELOCITY_FILTER_WEIGHT = 0.9f;
    private final boolean DEFAULT_ATTR_CLEAR_ON_DOUBLE_CLICK = false;

    private Paint mPaint = new Paint();
    private PixelMap mSignatureBitmap = null;
    private Canvas mSignatureBitmapCanvas = null;

    public float getmVelocityFilterWeight() {
        return mVelocityFilterWeight;
    }

    public OnSignedListener getmOnSignedListener() {
        return mOnSignedListener;
    }

    public SignaturePad(Context context) {
        super(context);
        init(null);
    }

    public SignaturePad(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public Paint getPaint() {
        return mPaint;
    }

    public void init(AttrSet attrSet) {
        if (attrSet != null) {
            Optional<Attr> penMinWidth = attrSet.getAttr("penMinWidth");
            mMinWidth = penMinWidth.isPresent() ? penMinWidth.get().getIntegerValue() : convertDpToPx(DEFAULT_ATTR_PEN_MIN_WIDTH_PX);

            Optional<Attr> penMaxWidth = attrSet.getAttr("penMaxWidth");
            mMaxWidth = penMaxWidth.isPresent() ? penMaxWidth.get().getIntegerValue() : convertDpToPx(DEFAULT_ATTR_PEN_MAX_WIDTH_PX);

            Optional<Attr> velocityFilterWeight = attrSet.getAttr("velocityFilterWeight");
            mVelocityFilterWeight = velocityFilterWeight.isPresent() ? velocityFilterWeight.get().getFloatValue() : DEFAULT_ATTR_VELOCITY_FILTER_WEIGHT;

            Optional<Attr> clearOnDoubleClick = attrSet.getAttr("clearOnDoubleClick");
            mClearOnDoubleClick = clearOnDoubleClick.isPresent() ? clearOnDoubleClick.get().getBoolValue() : DEFAULT_ATTR_CLEAR_ON_DOUBLE_CLICK;

            Optional<Attr> penColor = attrSet.getAttr("penColor");
            mPaint.setColor(penColor.isPresent() ? penColor.get().getColorValue() : DEFAULT_ATTR_PEN_COLOR);
        } else {
            mMinWidth = convertDpToPx(DEFAULT_ATTR_PEN_MIN_WIDTH_PX);
            mMaxWidth = convertDpToPx(DEFAULT_ATTR_PEN_MAX_WIDTH_PX);
            mVelocityFilterWeight = DEFAULT_ATTR_VELOCITY_FILTER_WEIGHT;
            mClearOnDoubleClick = DEFAULT_ATTR_CLEAR_ON_DOUBLE_CLICK;
            mPaint.setColor(DEFAULT_ATTR_PEN_COLOR);
        }


        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        mPaint.setStrokeJoin(Paint.Join.ROUND_JOIN);

        mDirtyRect = new RectFloat();
        clearView();
        addDrawTask(this);
        setTouchEventListener(this);
        setDoubleClickedListener(this);
    }

    /**
     * Set the pen color from a given resource.
     * If the resource is not found, {@link ohos.agp.utils.Color#BLACK} is assumed.
     *
     * @param colorRes the color resource.
     */
    public void setPenColorRes(int colorRes) {
        try {
            setPenColor(getResourceManager().getElement(colorRes).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
            setPenColor(Color.getIntColor("#000000"));
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the pen color from a given color.
     *
     * @param color the color.
     */
    public void setPenColor(int color) {
        mPaint.setColor(new Color(color));
    }

    /**
     * Set the minimum width of the stroke in pixel.
     *
     * @param minWidth the width in dp.
     */
    public void setMinWidth(float minWidth) {
        mMinWidth = convertDpToPx(minWidth);
    }

    public int getMinWidth() {
        return mMinWidth;
    }

    /**
     * Set the maximum width of the stroke in pixel.
     *
     * @param maxWidth the width in dp.
     */
    public void setMaxWidth(float maxWidth) {
        mMaxWidth = convertDpToPx(maxWidth);
    }

    public int getMaxWidth() {
        return mMaxWidth;
    }


    /**
     * Set the velocity filter weight.
     *
     * @param velocityFilterWeight the weight.
     */
    public void setVelocityFilterWeight(float velocityFilterWeight) {
        mVelocityFilterWeight = velocityFilterWeight;
    }


    public void clearView() {
        mSvgBuilder.clear();
        mPoints = new ArrayList<>();
        mLastVelocity = 0;
        mLastWidth = (mMinWidth + mMaxWidth) / 2;

        if (mSignatureBitmap != null) {
            mSignatureBitmap = null;
            ensureSignatureBitmap();
        }
        setIsEmpty(true);
        invalidate();
    }

    public void clear() {
        this.clearView();
        this.mHasEditState = true;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        MmiPoint point = event.getPointerPosition(0);
        float eventX = point.getX() ;
        float eventY = point.getY() ;

        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mPoints.clear();
                mLastTouchX = eventX;
                mLastTouchY = eventY;
                addPoint(getNewPoint(eventX, eventY));
                if (mOnSignedListener != null) mOnSignedListener.onStartSigning();

            case TouchEvent.POINT_MOVE:
                resetDirtyRect(eventX, eventY);
                addPoint(getNewPoint(eventX, eventY));
                setIsEmpty(false);
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                resetDirtyRect(eventX, eventY);
                addPoint(getNewPoint(eventX, eventY));
                break;

            default:
                return false;
        }
        invalidate();
        return true;
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mSignatureBitmap != null) {
            canvas.drawPixelMapHolder(new PixelMapHolder(mSignatureBitmap), 0, 0, mPaint);
        }
    }

    public void setOnSignedListener(OnSignedListener listener) {
        mOnSignedListener = listener;
    }

    public boolean isEmpty() {
        return mIsEmpty;
    }


    public String getSignatureSvg() {
        int width = getTransparentSignatureBitmap().getImageInfo().size.width;
        int height = getTransparentSignatureBitmap().getImageInfo().size.height;
        return mSvgBuilder.build(width, height);
    }


    public PixelMap getSignatureBitmap() {
        PixelMap originalBitmap = getTransparentSignatureBitmap();
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(originalBitmap.getImageInfo().size.width, originalBitmap.getImageInfo().size.height);
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        PixelMap whiteBgBitmap = PixelMap.create(initializationOptions);
        Texture texture = new Texture(whiteBgBitmap);
        Canvas canvas = new Canvas(texture);
        canvas.drawColor(Color.WHITE.getValue(), Canvas.PorterDuffMode.SRC);
        canvas.drawPixelMapHolder(new PixelMapHolder(originalBitmap), 0, 0, mPaint);
        return whiteBgBitmap;
    }


    public void setSignatureBitmap(final PixelMap signature) {
        clearView();
        ensureSignatureBitmap();

        RectFloat tempSrc = new RectFloat();
        RectFloat tempDst = new RectFloat();

        int dWidth = signature.getImageInfo().size.width;
        int dHeight = signature.getImageInfo().size.height;
        int vWidth = getWidth();
        int vHeight = getHeight();

        // Generate the required transform.
        tempSrc.fuse(0, 0, dWidth, dHeight);
        tempDst.fuse(0, 0, vWidth, vHeight);

        Matrix drawMatrix = new Matrix();
        drawMatrix.setRectToRect(tempSrc, tempDst, Matrix.ScaleToFit.CENTER);

        Texture texture = new Texture(mSignatureBitmap);
        Canvas canvas = new Canvas(texture);
        canvas.drawPixelMapHolderRect(new PixelMapHolder(signature), tempSrc, tempDst, null);
        setIsEmpty(false);
        invalidate();
    }


    public PixelMap getTransparentSignatureBitmap() {
        ensureSignatureBitmap();
        return mSignatureBitmap;
    }


    public PixelMap getTransparentSignatureBitmap(boolean trimBlankSpace) {
        if (!trimBlankSpace) {
            return getTransparentSignatureBitmap();
        }

        ensureSignatureBitmap();
        int imgHeight = mSignatureBitmap.getImageInfo().size.height;
        int imgWidth = mSignatureBitmap.getImageInfo().size.width;

        int backgroundColor = Color.TRANSPARENT.getValue();

        int xMin = Integer.MAX_VALUE,
                xMax = Integer.MIN_VALUE,
                yMin = Integer.MAX_VALUE,
                yMax = Integer.MIN_VALUE;

        boolean foundPixel = false;

        // Find xMin
        for (int x = 0; x < imgWidth; x++) {
            boolean stop = false;
            for (int y = 0; y < imgHeight; y++) {
                mSignatureBitmap.getPixelBytesCapacity();
                if (mSignatureBitmap.getImageInfo().pixelFormat.getValue() != backgroundColor) {
                    xMin = x;
                    stop = true;
                    foundPixel = true;
                    break;
                }
            }
            if (stop)
                break;
        }

        // Image is empty...
        if (!foundPixel)
            return null;

        // Find yMin
        for (int y = 0; y < imgHeight; y++) {
            boolean stop = false;
            for (int x = xMin; x < imgWidth; x++) {
                if (mSignatureBitmap.getImageInfo().pixelFormat.getValue() != backgroundColor) {
                    yMin = y;
                    stop = true;
                    break;
                }
            }
            if (stop)
                break;
        }

        // Find xMax
        for (int x = imgWidth - 1; x >= xMin; x--) {
            boolean stop = false;
            for (int y = yMin; y < imgHeight; y++) {
                if (mSignatureBitmap.getImageInfo().pixelFormat.getValue() != backgroundColor) {
                    xMax = x;
                    stop = true;
                    break;
                }
            }
            if (stop)
                break;
        }

        // Find yMax
        for (int y = imgHeight - 1; y >= yMin; y--) {
            boolean stop = false;
            for (int x = xMin; x <= xMax; x++) {
                if (mSignatureBitmap.getImageInfo().pixelFormat.getValue() != backgroundColor) {
                    yMax = y;
                    stop = true;
                    break;
                }
            }
            if (stop)
                break;
        }
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(getWidth(), getHeight());
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        return PixelMap.create(mSignatureBitmap, new Rect(xMin, yMin, xMax - xMin, yMax - yMin), initializationOptions);
    }


    private TimedPoint getNewPoint(float x, float y) {
        int mCacheSize = mPointsCache.size();
        TimedPoint timedPoint;
        if (mCacheSize == 0) {
            // Cache is empty, create a new point
            timedPoint = new TimedPoint();
        } else {
            // Get point from cache
            timedPoint = mPointsCache.remove(mCacheSize - 1);
        }
        return timedPoint.set(x, y);
    }


    private void recyclePoint(TimedPoint point) {
        mPointsCache.add(point);
    }

    private void addPoint(TimedPoint newPoint) {
        mPoints.add(newPoint);

        int pointsCount = mPoints.size();
        if (pointsCount > 3) {

            ControlTimedPoints tmp = calculateCurveControlPoints(mPoints.get(0), mPoints.get(1), mPoints.get(2));
            TimedPoint c2 = tmp.c2;
            recyclePoint(tmp.c1);

            tmp = calculateCurveControlPoints(mPoints.get(1), mPoints.get(2), mPoints.get(3));
            TimedPoint c3 = tmp.c1;
            recyclePoint(tmp.c2);

            Bezier curve = mBezierCached.set(mPoints.get(1), c2, c3, mPoints.get(2));

            TimedPoint startPoint = curve.startPoint;
            TimedPoint endPoint = curve.endPoint;

            float velocity = endPoint.velocityFrom(startPoint);
            velocity = Float.isNaN(velocity) ? 0.0f : velocity;

            velocity = mVelocityFilterWeight * velocity
                    + (1 - mVelocityFilterWeight) * mLastVelocity;

            // The new width is a function of the velocity. Higher velocities
            // correspond to thinner strokes.
            float newWidth = strokeWidth(velocity);

            // The Bezier's width starts out as last curve's final width, and
            // gradually changes to the stroke width just calculated. The new
            // width calculation is based on the velocity between the Bezier's
            // start and end mPoints.
            addBezier(curve, mLastWidth, newWidth);

            mLastVelocity = velocity;
            mLastWidth = newWidth;

            // Remove the first element from the list,
            // so that we always have no more than 4 mPoints in mPoints array.
            recyclePoint(mPoints.remove(0));

            recyclePoint(c2);
            recyclePoint(c3);

        } else if (pointsCount == 1) {
            // To reduce the initial lag make it work with 3 mPoints
            // by duplicating the first point
            TimedPoint firstPoint = mPoints.get(0);
            mPoints.add(getNewPoint(firstPoint.x, firstPoint.y));
        }
        this.mHasEditState = true;
    }

    private void addBezier(Bezier curve, float startWidth, float endWidth) {
        mSvgBuilder.append(curve, (startWidth + endWidth) / 2);
        ensureSignatureBitmap();
        float originalWidth = mPaint.getStrokeWidth();
        float widthDelta = endWidth - startWidth;
        float drawSteps = (float) Math.ceil(curve.length());

        for (int i = 0; i < drawSteps; i++) {
            // Calculate the Bezier (x, y) coordinate for this step.
            float t = ((float) i) / drawSteps;
            float tt = t * t;
            float ttt = tt * t;
            float u = 1 - t;
            float uu = u * u;
            float uuu = uu * u;

            float x = uuu * curve.startPoint.x;
            x += 3 * uu * t * curve.control1.x;
            x += 3 * u * tt * curve.control2.x;
            x += ttt * curve.endPoint.x;

            float y = uuu * curve.startPoint.y;
            y += 3 * uu * t * curve.control1.y;
            y += 3 * u * tt * curve.control2.y;
            y += ttt * curve.endPoint.y;

            // Set the incremental stroke width and draw.
            mPaint.setStrokeWidth(startWidth + ttt * widthDelta);
            mSignatureBitmapCanvas.drawPoint(x, y, mPaint);
            expandDirtyRect(x, y);
        }

        mPaint.setStrokeWidth(originalWidth);
    }

    private ControlTimedPoints calculateCurveControlPoints(TimedPoint s1, TimedPoint s2, TimedPoint s3) {
        float dx1 = s1.x - s2.x;
        float dy1 = s1.y - s2.y;
        float dx2 = s2.x - s3.x;
        float dy2 = s2.y - s3.y;

        float m1X = (s1.x + s2.x) / 2.0f;
        float m1Y = (s1.y + s2.y) / 2.0f;
        float m2X = (s2.x + s3.x) / 2.0f;
        float m2Y = (s2.y + s3.y) / 2.0f;

        float l1 = (float) Math.sqrt(dx1 * dx1 + dy1 * dy1);
        float l2 = (float) Math.sqrt(dx2 * dx2 + dy2 * dy2);

        float dxm = (m1X - m2X);
        float dym = (m1Y - m2Y);
        float k = l2 / (l1 + l2);
        if (Float.isNaN(k)) k = 0.0f;
        float cmX = m2X + dxm * k;
        float cmY = m2Y + dym * k;

        float tx = s2.x - cmX;
        float ty = s2.y - cmY;

        return mControlTimedPointsCached.set(getNewPoint(m1X + tx, m1Y + ty), getNewPoint(m2X + tx, m2Y + ty));
    }

    private float strokeWidth(float velocity) {
        return Math.max(mMaxWidth / (velocity + 1), mMinWidth);
    }

    /**
     * Called when replaying history to ensure the dirty region includes all
     * mPoints.
     *
     * @param historicalX the previous x coordinate.
     * @param historicalY the previous y coordinate.
     */
    private void expandDirtyRect(float historicalX, float historicalY) {
        if (historicalX < mDirtyRect.left) {
            mDirtyRect.left = historicalX;
        } else if (historicalX > mDirtyRect.right) {
            mDirtyRect.right = historicalX;
        }
        if (historicalY < mDirtyRect.top) {
            mDirtyRect.top = historicalY;
        } else if (historicalY > mDirtyRect.bottom) {
            mDirtyRect.bottom = historicalY;
        }
    }

    /**
     * Resets the dirty region when the motion event occurs.
     *
     * @param eventX the event x coordinate.
     * @param eventY the event y coordinate.
     */
    private void resetDirtyRect(float eventX, float eventY) {
        // The mLastTouchX and mLastTouchY were set when the ACTION_DOWN motion event occurred.
        mDirtyRect.left = Math.min(mLastTouchX, eventX);
        mDirtyRect.right = Math.max(mLastTouchX, eventX);
        mDirtyRect.top = Math.min(mLastTouchY, eventY);
        mDirtyRect.bottom = Math.max(mLastTouchY, eventY);
    }

    public void setIsEmpty(boolean newValue) {
        mIsEmpty = newValue;
        if (mOnSignedListener != null) {
            if (mIsEmpty) {
                mOnSignedListener.onClear();
            } else {
                mOnSignedListener.onSigned();
            }
        }
    }

    private void ensureSignatureBitmap() {
        if (mSignatureBitmap == null) {
            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
            initializationOptions.size = new Size(getWidth(), getHeight());
            initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
            mSignatureBitmap = PixelMap.create(initializationOptions);
            Texture texture = new Texture(mSignatureBitmap);
            mSignatureBitmapCanvas = new Canvas(texture);
        }
    }

    public int convertDpToPx(float dp) {
        return Math.round(getResourceManager().getDeviceCapability().screenDensity / 160 * dp);
    }

    @Override
    public void onDoubleClick(Component component) {
        HiLog.info(label, "onDoubleClick");
        if (mClearOnDoubleClick) {
            clearView();
        }
    }

    public interface OnSignedListener {
        void onStartSigning();

        void onSigned();

        void onClear();
    }

    public boolean ismIsEmpty() {
        return mIsEmpty;
    }

    public Boolean getmHasEditState() {
        return mHasEditState;
    }

}
