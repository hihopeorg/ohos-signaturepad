package com.github.gcacace.signaturepad.view;

import ohos.agp.components.Component;

public class ViewCompat {

    /**Returns true if {@code view} has been through at least one layout since it
     * Returns true if {@code view} has been through at least one layout since it
     * was last attached to or detached from a window.
     *
     *
     * @param view the view
     * @return true if this view has been through at least one layout since it was last attached to or detached from a window.
     */
    public static boolean isLaidOut(Component view) {
        return view.getWidth() > 0 && view.getHeight() > 0;
    }
}
